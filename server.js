var http = require('http');
var express = require('express');
var ip = require("ip");

var app = express();
app.set('view engine', 'jade');
app.set('views', './views');
app.use(express.static('public'));


app.get('/', function (req, res) {
  res.render('index', { title: 'Hey', message: 'Hello there!'});
});

if(ip.address()=='192.168.178.10')
{
	var server = http.createServer(app).listen(8080,'127.0.0.1');
}else{
	var server = http.createServer(app).listen(80);
}


console.log('Server running.');

